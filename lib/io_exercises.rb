# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  number = rand(1..100)
  guesses = 0
  loop do
    puts("Guess a number")
    guess = gets.chomp.to_i
    puts(guess)
    guesses += 1
    case guess <=> number
      when -1
        puts("too low")
      when 0
        puts("Correct! Took #{guesses} guesses.")
        break
      when 1
        puts("too high")
    end
  end
end

def shuffle_file
  puts("Input a file to shuffle")
  input = gets.chomp
  input_contents = File.readlines(input)
  shuffled_contents = input_contents.shuffle
  output = input[0...input.index('.')] + '-shuffled' + input[input.index('.')..-1]
  File.open(output, 'w') { |f| f.puts(shuffled_contents.join) }
  puts("Output shuffled file to #{output}")
end
